# Bilan CODE-SHS 2020-2024

Retrouvez les différentes actions réalisées dans le cadre du projet CODE-SHS entre 2020 et 2024.

Le projet CODE-SHS pour *Contribuer à l'Ouverture des Données* est porté par le réseau SO-MATé.

Bonne lecture :-)